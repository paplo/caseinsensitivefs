#!/usr/bin/env python
# This file is part of CaseSensitiveFS
#
# Copyright (C) 2017-2018 Pawel Filipczak <paplo@taski.com.pl>
#
# CaseSensitiveFS is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 3 of the License, or (at your option) any later version.
#
# Alternatively, you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of
# the License, or (at your option) any later version.
#
# CaseSensitiveFS is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License or the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License and a copy of the GNU General Public License along with
# CaseSensitiveFS. If not, see <http://www.gnu.org/licenses/>.

from __future__ import with_statement

import os
import sys
import errno

from fuse import FUSE, FuseOSError, Operations

#import inspect
debug = False



class Passthrough(Operations):
    def __init__(self, root):
        self.root = root

    # Helpers
    # =======

    def _find_ok(self, partial, item):
        path = self.root + partial
        path_with_item =  path + "/" + item
        if not os.access(path_with_item, os.F_OK):
            for f in os.listdir(path):
                if isinstance(f, unicode):
                    if f.lower() == item.lower():
                        return f
                else:
                    if unicode(f.lower(), encoding="utf-8") == item.lower():
                        return f
        return item

    def _split(self, partial):
        if partial.startswith("/"):
            partial = partial[1:]

        splitted = partial.split("/")

        rp = ""
        if len(splitted) == 0:
            return partial

        for i in range(0, len(splitted)):
            sp = splitted[i]
            item = self._find_ok(rp, sp)
            splitted[i] = item
            rp = rp + "/" + item
        return '/'.join(splitted)


    def _full_path(self, partial):
        partial = self._split(partial)

        if partial.startswith("/"):
            partial = partial[1:]
        path = os.path.join(self.root, partial)

        return path

    # Filesystem methods
    # ==================

    def access(self, path, mode):
        if debug == True:
            print inspect.stack()[0][3]

        full_path = self._full_path(path)
        if not os.access(full_path, mode):
            raise FuseOSError(errno.EACCES)

    def chmod(self, path, mode):
        if debug == True:
            print inspect.stack()[0][3]
        full_path = self._full_path(path)
        return os.chmod(full_path, mode)

    def chown(self, path, uid, gid):
        if debug == True:
            print inspect.stack()[0][3]
        full_path = self._full_path(path)
        return os.chown(full_path, uid, gid)

    def getattr(self, path, fh=None):
        if debug == True:
            print inspect.stack()[0][3]

        full_path = self._full_path(path)
        st = os.lstat(full_path)
        return dict((key, getattr(st, key)) for key in ('st_atime', 'st_ctime',
                     'st_gid', 'st_mode', 'st_mtime', 'st_nlink', 'st_size', 'st_uid'))

    def readdir(self, path, fh):
        if debug == True:
            print inspect.stack()[0][3]
        full_path = self._full_path(path)

        dirents = ['.', '..']
        if os.path.isdir(full_path):
            dirents.extend(os.listdir(full_path))
        for r in dirents:
            r = r.lower()
            yield r

    def readlink(self, path):
        if debug == True:
            print inspect.stack()[0][3]
        pathname = os.readlink(self._full_path(path))
        if pathname.startswith("/"):
            # Path name is absolute, sanitize it.
            return os.path.relpath(pathname, self.root)
        else:
            return pathname

    def mknod(self, path, mode, dev):
        if debug == True:
            print inspect.stack()[0][3]
        return os.mknod(self._full_path(path), mode, dev)

    def rmdir(self, path):
        if debug == True:
            print inspect.stack()[0][3]
        full_path = self._full_path(path)
        return os.rmdir(full_path)

    def mkdir(self, path, mode):
        if debug == True:
            print inspect.stack()[0][3]
        return os.mkdir(self._full_path(path), mode)

    def statfs(self, path):
        if debug == True:
            print inspect.stack()[0][3]
        full_path = self._full_path(path)
        stv = os.statvfs(full_path)
        return dict((key, getattr(stv, key)) for key in ('f_bavail', 'f_bfree',
            'f_blocks', 'f_bsize', 'f_favail', 'f_ffree', 'f_files', 'f_flag',
            'f_frsize', 'f_namemax'))

    def unlink(self, path):
        if debug == True:
            print inspect.stack()[0][3]
        return os.unlink(self._full_path(path))

    def symlink(self, name, target):
        if debug == True:
            print inspect.stack()[0][3]
        return os.symlink(name, self._full_path(target))

    def rename(self, old, new):
        if debug == True:
            print inspect.stack()[0][3]
        return os.rename(self._full_path(old), self._full_path(new))

    def link(self, target, name):
        if debug == True:
            print inspect.stack()[0][3]
        return os.link(self._full_path(target), self._full_path(name))

    def utimens(self, path, times=None):
        if debug == True:
            print inspect.stack()[0][3]
        return os.utime(self._full_path(path), times)

    # File methods
    # ============

    def open(self, path, flags):
        if debug == True:
            print inspect.stack()[0][3]
        full_path = self._full_path(path)
        return os.open(full_path, flags)

    def create(self, path, mode, fi=None):
        if debug == True:
            print inspect.stack()[0][3]
        full_path = self._full_path(path)
        return os.open(full_path, os.O_WRONLY | os.O_CREAT, mode)

    def read(self, path, length, offset, fh):
        if debug == True:
            print inspect.stack()[0][3]
        os.lseek(fh, offset, os.SEEK_SET)
        return os.read(fh, length)

    def write(self, path, buf, offset, fh):
        if debug == True:
            print inspect.stack()[0][3]
        os.lseek(fh, offset, os.SEEK_SET)
        return os.write(fh, buf)

    def truncate(self, path, length, fh=None):
        if debug == True:
            print inspect.stack()[0][3]
        full_path = self._full_path(path)
        with open(full_path, 'r+') as f:
            f.truncate(length)

    def flush(self, path, fh):
        if debug == True:
            print inspect.stack()[0][3]
        return os.fsync(fh)

    def release(self, path, fh):
        if debug == True:
            print inspect.stack()[0][3]
        return os.close(fh)

    def fsync(self, path, fdatasync, fh):
        if debug == True:
            print inspect.stack()[0][3]
        return self.flush(path, fh)


def main(mountpoint, root):
    FUSE(Passthrough(root), mountpoint, nothreads=True, foreground=True)

if __name__ == '__main__':
    main(sys.argv[2], sys.argv[1])
